﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EntidadBancaria.cs" company="Pascual">
//   Pascual Maestre Server.
// </copyright>
// <summary>
//   Defines the EntidadBancaria type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GestionEntidadesBancarias.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Clase entidad bancaria 
    /// </summary>
    public class EntidadBancaria
    {
        #region Datos Entidad

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the nombre.
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Gets or sets the direccion.
        /// </summary>
        public string Direccion { get; set; }

        /// <summary>
        /// Gets or sets the poblacion.
        /// </summary>
        public string Poblacion { get; set; }

        /// <summary>
        /// Gets or sets the provincia.
        /// </summary>
        public string Provincia { get; set; }

        /// <summary>
        /// Gets or sets the cod postal.
        /// </summary>
        public string CodPostal { get; set; }

        /// <summary>
        /// Gets or sets the telefono.
        /// </summary>
        [Phone]
        public string Telefono { get; set; }

        /// <summary>
        /// Gets or sets the mail.
        /// </summary>
        [EmailAddress]
        public string Mail { get; set; }

        /// <summary>
        /// Gets or sets the logo.
        /// </summary>
        public byte[] Logo { get; set; }

        /// <summary>
        /// Gets or sets the pais.
        /// </summary>
        public string Pais { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether estado_ activo.
        /// --------
        /// La columna estado_activo es un filtro global booleano
        /// que indica si debe mostrar ese registro o no y afecta a todas las consultas.
        /// </summary>
        public bool Estado_Activo { get; set; }

        #endregion

        #region Relaciones

        /// <summary>
        /// Gets or sets la ID del grupo de entidades al que pertenece.
        /// </summary>
        public Guid GrupoEntidadId { get; set; }

        /// <summary>
        /// Gets or sets el grupo de entidad al que pertenece.
        /// </summary>
        public GrupoEntidad Grupo { get; set; }

        #endregion
    }
}
