﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GestionEntidadesContext.cs" company="Pascual">
//   Pascual Maestre Server
// </copyright>
// <summary>
//   Defines the GestionEntidadesContext type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GestionEntidadesBancarias.Models
{
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The gestion entidades context.
    /// </summary>
    public class GestionEntidadesContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GestionEntidadesContext"/> class.
        /// </summary>
        /// <param name="options">
        /// The options.
        /// </param>
        public GestionEntidadesContext(DbContextOptions<GestionEntidadesContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets the entidades bancarias.
        /// </summary>
        public DbSet<EntidadBancaria> EntidadesBancarias { get; set; }

        /// <summary>
        /// Gets or sets the grupo entidad.
        /// </summary>
        public DbSet<GrupoEntidad> GrupoEntidad { get; set; }
    }
}
