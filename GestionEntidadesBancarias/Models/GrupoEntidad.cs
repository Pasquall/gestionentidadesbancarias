﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GrupoEntidad.cs" company="Pascual">
//   Pascual Maestre Server
// </copyright>
// <summary>
//   Defines the GrupoEntidad type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GestionEntidadesBancarias.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Un “grupo de entidad” engloba muchas entidades
    /// </summary>
    public class GrupoEntidad
    {
        #region Datos

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the nombre.
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        public string Color { get; set; }

        #endregion

        #region Relaciones

        /// <summary>
        /// Gets or sets the entidades.
        /// </summary>
        public ICollection<EntidadBancaria> Entidades { get; set; }

        #endregion
    }
}
