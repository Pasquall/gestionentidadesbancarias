﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GrupoEntidadData.cs" company="Pascual">
//   Pascual Maestre Server.
// </copyright>
// <summary>
//   The grupo entidad data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GestionEntidadesBancarias.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using GestionEntidadesBancarias.Models;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The grupo entidad data.
    /// </summary>
    public class GrupoEntidadData : AbstractGenericGestionEntidadesData<GrupoEntidad>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GrupoEntidadData"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public GrupoEntidadData(GestionEntidadesContext context)
            : base(context)
        {
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<GrupoEntidad> GetAll()
        {
            var query = this.GetAllIncludes().OrderBy(e => e.Id);

            return query;
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="GrupoEntidad"/>.
        /// </returns>
        public GrupoEntidad GetById(Guid id)
        {
            var query = this.GetAllIncludes().Where(g => g.Id == id);

            return query.FirstOrDefault();
        }

        /// <summary>
        /// The do create.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        protected override void DoCreate(GrupoEntidad data)
        {
            this.DbContext.GrupoEntidad.Add(data);
        }

        /// <summary>
        /// The do update.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        protected override void DoUpdate(GrupoEntidad data)
        {
            this.DbContext.Entry(data).State = EntityState.Modified;
        }

        /// <summary>
        /// The do delete.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        protected override void DoDelete(GrupoEntidad data)
        {
            this.DbContext.Entry(data).State = EntityState.Deleted;
        }

        /// <summary>
        /// The get all includes.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        private IEnumerable<GrupoEntidad> GetAllIncludes()
        {
            return this.DbContext.GrupoEntidad;
        }
    }
}
