﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AbstractGenericGestionEntidadesData.cs" company="Pascual">
//   Pascual Maestre Server.
// </copyright>
// <summary>
//   Defines the AbstractGenericGestionEntidadesData type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GestionEntidadesBancarias.Data
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Transactions;

    using GestionEntidadesBancarias.Models;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The abstract generic gestion entidades data.
    /// </summary>
    /// <typeparam name="TData">
    /// Type of data que se va a usar
    /// </typeparam>
    [SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1202:ElementsMustBeOrderedByAccess", Justification = "Reviewed. Suppression is OK here.")]
    public abstract class AbstractGenericGestionEntidadesData<TData>
        where TData : class
    {
        /// <summary>
        /// The db context.
        /// </summary>
        protected readonly GestionEntidadesContext DbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractGenericGestionEntidadesData{TData}"/> class. 
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        protected AbstractGenericGestionEntidadesData(GestionEntidadesContext context)
        {
            this.DbContext = context;
        }

        #region Create

        /// <summary>
        /// Gets or sets the data set.
        /// </summary>
        protected DbSet<TData> DataSet { get; set; }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool Create(TData data)
        {
            return this.ExecuteDatabaseAction(data, this.DoCreate);
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool Create(IEnumerable<TData> data)
        {
            return this.ExecuteDatabaseAction(data, this.DoCreate);
        }

        /// <summary>
        /// The do create.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        protected abstract void DoCreate(TData data);

        #endregion

        #region Update

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool Update(TData data)
        {
            return this.ExecuteDatabaseAction(data, this.DoUpdate);
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool Update(IEnumerable<TData> data)
        {
            return this.ExecuteDatabaseAction(data, this.DoUpdate);
        }

        /// <summary>
        /// The do update.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        protected abstract void DoUpdate(TData data);

        #endregion

        #region Delete

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool Delete(TData data)
        {
            return this.ExecuteDatabaseAction(data, this.DoDelete);
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool Delete(IEnumerable<TData> data)
        {
            return this.ExecuteDatabaseAction(data, this.DoDelete);
        }

        /// <summary>
        /// The do delete.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        protected abstract void DoDelete(TData data);

        #endregion

        #region ExecuteAction

        /// <summary>
        /// The execute database action.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="doDatabaseAction">
        /// The do database action.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        protected virtual bool ExecuteDatabaseAction(TData data, Action<TData> doDatabaseAction)
        {
            try
            {
                if (data != null)
                {
                    doDatabaseAction(data);

                    this.DbContext.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// The execute database action.
        /// </summary>
        /// <param name="dataEnumerable">
        /// The data enumerable.
        /// </param>
        /// <param name="doDatabaseAction">
        /// The do database action.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        protected virtual bool ExecuteDatabaseAction(IEnumerable<TData> dataEnumerable, Action<TData> doDatabaseAction)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                var ok = dataEnumerable.All(data => this.ExecuteDatabaseAction(data, doDatabaseAction));

                if (ok)
                {
                    scope.Complete();
                }

                return ok;
            }
        }

        #endregion
    }
}