﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EntidadBancariaData.cs" company="Pascual">
//   Pascual Maestre Server.
// </copyright>
// <summary>
//   The entidad bancaria data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GestionEntidadesBancarias.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using GestionEntidadesBancarias.Models;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The entidad bancaria data.
    /// </summary>
    public class EntidadBancariaData : AbstractGenericGestionEntidadesData<EntidadBancaria>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EntidadBancariaData"/> class. 
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public EntidadBancariaData(GestionEntidadesContext context)
            : base(context)
        {
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="poblacion">
        /// The poblacion. Optional filter value.
        /// </param>
        /// <param name="provincia">
        /// The provincia. Optional filter value.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<EntidadBancaria> GetAll(string poblacion = null, string provincia = null)
        {
            var query = this.GetAllIncludes();

            var entidadBancarias = query.ToList();
            IEnumerable<EntidadBancaria> queryFilter = entidadBancarias;
            if (poblacion != null)
            {
                queryFilter = queryFilter.Where(e => e.Poblacion.ToLower().Contains(poblacion.ToLower()));
            }

            if (provincia != null)
            {
                queryFilter = queryFilter.Where(e => e.Provincia.ToLower().Contains(provincia.ToLower()));
            }

            return queryFilter.OrderBy(e => e.Id);
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="GrupoEntidad"/>.
        /// </returns>
        public EntidadBancaria GetById(Guid id)
        {
            var query = this.GetAllIncludes().Where(g => g.Id == id);

            return query.FirstOrDefault();
        }

        /// <summary>
        /// The do create.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        protected override void DoCreate(EntidadBancaria data)
        {
            this.DbContext.EntidadesBancarias.Add(data);
        }

        /// <summary>
        /// The do update.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        protected override void DoUpdate(EntidadBancaria data)
        {
            this.DbContext.Entry(data).State = EntityState.Modified;
        }

        /// <summary>
        /// The do delete.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        protected override void DoDelete(EntidadBancaria data)
        {
            this.DbContext.Entry(data).State = EntityState.Deleted;
        }

        /// <summary>
        /// The get all includes.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        private IEnumerable<EntidadBancaria> GetAllIncludes()
        {
            return this.DbContext.EntidadesBancarias;
        }
    }
}
