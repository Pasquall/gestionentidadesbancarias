﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="20190419173326_InitialCreate.cs" company="Pascual">
//   Pascual Maestre Server.
// </copyright>
// <summary>
//   Defines the InitialCreate type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GestionEntidadesBancarias.Migrations
{
    using System;

    using Microsoft.EntityFrameworkCore.Migrations;

    /// <summary>
    /// The initial create.
    /// </summary>
    public partial class InitialCreate : Migration
    {
        /// <summary>
        /// The up.
        /// </summary>
        /// <param name="migrationBuilder">
        /// The migration builder.
        /// </param>
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GrupoEntidad",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nombre = table.Column<string>(nullable: true),
                    Color = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GrupoEntidad", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EntidadesBancarias",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nombre = table.Column<string>(nullable: true),
                    Direccion = table.Column<string>(nullable: true),
                    Poblacion = table.Column<string>(nullable: true),
                    Provincia = table.Column<string>(nullable: true),
                    CodPostal = table.Column<string>(nullable: true),
                    Telefono = table.Column<string>(nullable: true),
                    Mail = table.Column<string>(nullable: true),
                    Logo = table.Column<byte[]>(nullable: true),
                    Pais = table.Column<string>(nullable: true),
                    Estado_Activo = table.Column<bool>(nullable: false),
                    GrupoEntidadId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EntidadesBancarias", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EntidadesBancarias_GrupoEntidad_GrupoEntidadId",
                        column: x => x.GrupoEntidadId,
                        principalTable: "GrupoEntidad",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EntidadesBancarias_GrupoEntidadId",
                table: "EntidadesBancarias",
                column: "GrupoEntidadId");
        }

        /// <summary>
        /// The down.
        /// </summary>
        /// <param name="migrationBuilder">
        /// The migration builder.
        /// </param>
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EntidadesBancarias");

            migrationBuilder.DropTable(
                name: "GrupoEntidad");
        }
    }
}
