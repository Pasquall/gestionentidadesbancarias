﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GrupoEntidadController.cs" company="Pascual">
//   Pascual Maestre Server.
// </copyright>
// <summary>
//   Defines the GrupoEntidadController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GestionEntidadesBancarias.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using GestionEntidadesBancarias.Data;
    using GestionEntidadesBancarias.Models;

    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The grupo entidad controller.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class GrupoEntidadController : GestionEntidadesController
    {
        /// <summary>
        /// The grupo entidad data.
        /// </summary>
        private readonly GrupoEntidadData grupoEntidadData;

        /// <summary>
        /// Initializes a new instance of the <see cref="GrupoEntidadController"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public GrupoEntidadController(GestionEntidadesContext context)
            : base(context)
        {
            this.grupoEntidadData = new GrupoEntidadData(context);
        }

        /// <summary>
        /// GET: api/<controller>.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpGet]
        public ActionResult<IEnumerable<GrupoEntidad>> Get()
        {
            try
            {
                return this.grupoEntidadData.GetAll().ToList();
            }
            catch (Exception e)
            {
                return this.BadRequest(e.InnerException.Message);
            }
        }

        /// <summary>
        /// GET api/<controller>/<guidNumber>.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpGet("{id}")]
        public ActionResult<GrupoEntidad> Get(Guid id)
        {
            try
            {
                var grupoEntidad = this.grupoEntidadData.GetById(id);

                if (grupoEntidad == null)
                {
                    return this.NotFound();
                }

                return grupoEntidad;
            }
            catch (Exception e)
            {
                return this.BadRequest(e.InnerException.Message);
            }
        }

        /// <summary>
        /// POST api/<controller>.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult<GrupoEntidad> Create(GrupoEntidad model)
        {
            try
            {
                if (!this.ModelState.IsValid)
                {
                    return this.BadRequest(this.ModelState);
                }

                this.grupoEntidadData.Create(model);

                return this.CreatedAtAction(nameof(this.Get), new { id = model.Id }, model);

            }
            catch (Exception e)
            {
                return this.BadRequest(e.InnerException.Message);
            }
        }

        /// <summary>
        /// The put.
        /// </summary>
        /// <param name="id">
        ///  PUT api/<controller>/<GuidNumber>.
        /// </param>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, GrupoEntidad model)
        {
            if (id != model.Id)
            {
                return this.BadRequest();
            }

            try
            {
                if (!this.ModelState.IsValid)
                {
                    return this.BadRequest(this.ModelState);
                }

                this.grupoEntidadData.Update(model);

                return this.NoContent();
            }
            catch (Exception e)
            {
                return this.BadRequest(e.InnerException.Message);
            }
        }

        /// <summary>
        /// DELETE api/<controller>/<GuidNumber>.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            try
            {
                var grupoEntidad = this.DbContext.GrupoEntidad.Find(id);

                if (grupoEntidad == null)
                {
                    return this.NotFound();
                }

                this.grupoEntidadData.Delete(grupoEntidad);

                return this.NoContent();
            }
            catch (Exception e)
            {
                return this.BadRequest(e.InnerException.Message);
            }
        }
    }
}
