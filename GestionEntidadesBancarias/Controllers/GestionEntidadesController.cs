﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GestionEntidadesController.cs" company="Pascual">
//   Pascual Maestre Server.
// </copyright>
// <summary>
//   Defines the GestionEntidadesController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GestionEntidadesBancarias.Controllers
{
    using GestionEntidadesBancarias.Models;

    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The gestion entidades controller.
    /// </summary>
    public abstract class GestionEntidadesController : Controller
    {
        /// <summary>
        /// The _context.
        /// </summary>
        protected readonly GestionEntidadesContext DbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="GestionEntidadesController"/> class. 
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        protected GestionEntidadesController(GestionEntidadesContext context)
        {
            this.DbContext = context;
        }
    }
}
