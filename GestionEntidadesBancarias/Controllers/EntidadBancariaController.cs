﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EntidadBancariaController.cs" company="Pascual">
//   Pascual Maestre Server.
// </copyright>
// <summary>
//   Defines the EntidadBancariaController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GestionEntidadesBancarias.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using GestionEntidadesBancarias.Data;
    using GestionEntidadesBancarias.Models;

    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Controlador para el modelo Entidad Bancaria
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class EntidadBancariaController : GestionEntidadesController
    {
        /// <summary>
        /// The entidad bancaria data.
        /// </summary>
        private readonly EntidadBancariaData entidadBancariaData;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntidadBancariaController"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public EntidadBancariaController(GestionEntidadesContext context)
            : base(context)
        {
            this.entidadBancariaData = new EntidadBancariaData(context);
        }

        /// <summary>
        /// GET: api/<controller>
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        [HttpGet]
        public ActionResult<IEnumerable<EntidadBancaria>> Get(string poblacion = null, string provincia = null)
        {
            try
            {
                return this.entidadBancariaData.GetAll(poblacion, provincia).ToList();
            }
            catch (Exception e)
            {
                return this.BadRequest(e.InnerException.Message);
            }
        }

        /// <summary>
        /// GET api/<controller>/<guidNumber>
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpGet("{id}")]
        public ActionResult<EntidadBancaria> Get(Guid id)
        {
            try
            {
                var entidadBancaria = this.entidadBancariaData.GetById(id);

                if (entidadBancaria == null)
                {
                    return this.NotFound();
                }

                return entidadBancaria;
            }
            catch (Exception e)
            {
                return this.BadRequest(e.InnerException.Message);
            }
        }

        /// <summary>
        /// POST api/<controller>.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost("Create")]
        public ActionResult<EntidadBancaria> Create(EntidadBancaria model)
        {
            try
            {
                if (!this.ModelState.IsValid)
                {
                    return this.BadRequest(this.ModelState);
                }

                this.entidadBancariaData.Create(model);

                return this.CreatedAtAction(nameof(this.Get), new { id = model.Id }, model);
            }
            catch (Exception e)
            {
                return this.BadRequest(e.InnerException.Message);
            }
        }

        /// <summary>
        /// PUT api/<controller>/<GuidNumber>.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, EntidadBancaria model)
        {
            if (id != model.Id)
            {
                return this.BadRequest();
            }

            try
            {
                if (!this.ModelState.IsValid)
                {
                    return this.BadRequest(this.ModelState);
                }

                this.entidadBancariaData.Update(model);

                return this.NoContent();
            }
            catch (Exception e)
            {
                return this.BadRequest(e.InnerException.Message);
            }
        }

        /// <summary>
        /// DELETE api/<controller>/<GuidNumber>.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            try
            {
                var entidadBancaria = this.DbContext.EntidadesBancarias.Find(id);

                if (entidadBancaria == null)
                {
                    return this.NotFound();
                }

                this.entidadBancariaData.Delete(entidadBancaria);

                return this.NoContent();
            }
            catch (Exception e)
            {
                return this.BadRequest(e.InnerException.Message);
            }
        }
    }
}
